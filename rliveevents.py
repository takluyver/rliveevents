"""This module runs continuous updates for R, such as redrawing graphs when
the plot window is resized. Use the start() and stop() functions to turn
updates on and off.

The interval variable determines how often updates are performed. The default
is every 0.2 seconds.

Importing the module will automatically start updates.
"""
from rpy2.rinterface import process_revents
import time
import threading

# If this is True, start updates when the module is imported
START_INTERACTIVE = True

class _ReventThread(threading.Thread):
    """The thread for processing R events. Use the module's start() and stop()
    functions to switch it on and off. If you interact with this class
    directly, it's up to you to ensure you don't start several of them."""
    daemon = True
    def __init__(self):
        super(_ReventThread, self).__init__()
        self.running = True
        
    def stop(self):
        self.running = False
        self.join()
        
    def run(self):
        while self.running:
            try:
                process_revents()
            except RuntimeError:
                pass
            time.sleep(interval)

_eventthread = _ReventThread()
interval = 0.2

def start():
    """Start running continuous updates of plots etc. If updates are already
    running, this has no effect."""
    global _eventthread
    if _eventthread.is_alive():
        return
    _eventthread.start()
    
def stop():
    """Stop continuous updates. If updates aren't running, this will do
    nothing."""
    global _eventthread
    if not _eventthread.is_alive():
        return
    _eventthread.stop()
    # Create a new thread so it can be started again
    _eventthread = _ReventThread()

if START_INTERACTIVE:
    start()
